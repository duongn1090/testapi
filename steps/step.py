from behave import *
import json
import requests
from nose.tools import assert_equal


@when('I send a POST request to "{url_path_segment}" with parameter key "{key}" and value "{value}"')
def step_impl(context, url_path_segment, key, value):
    payload = {key: value}
    context.response = requests.post(url_path_segment, params=payload)


@when('I send a GET request to "{url_path_segment}" with parameter key "{key}" and value "{value}"')
def get_request(context, url_path_segment, key, value):
    payload = {key: value}
    context.response = requests.get(url_path_segment, params=payload)


@then('the response status should be "{status}"')
def response_status(context, status):
    assert_equal(context.response.status_code, int(status))


@then('the output should be "{expected_output}"')
def output_should_be(context, expected_output):
    expected_json = json.loads(expected_output)
    assert_equal(context.response.json(), expected_json)

