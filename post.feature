Feature: As a REST API
  I can send POST request with parameter

  Scenario: Send a POST request with blank input
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value " "
      Then the response status should be "200"
      And the output should be "false"

  Scenario: Send a POST request with parameter is character or special symbol like $
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "$"
      Then the response status should be "200"
      And the output should be "Invalid Input"

  Scenario: Send a POST request with parameter is 2 numbers
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "3, 4"
      Then the response status should be "200"
      And the output should be "Invalid Input"

  Scenario: Send a POST request with parameter is 0
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "0"
      Then the response status should be "200"
      And the output should be "Invalid Input"

  Scenario: Send a POST request with parameter is 1001
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "1001"
      Then the response status should be "200"
      And the output should be "Invalid Input"

  Scenario: Send a POST request with parameter is 1
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "1"
      Then the response status should be "200"
      And the output should be "false"

  Scenario: Send a POST request with parameter is 2
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "2"
      Then the response status should be "200"
      And the output should be "true"

  Scenario: Send a POST request with parameter is 4
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "4"
      Then the response status should be "200"
      And the output should be "false"

  Scenario: Send a POST request with parameter is 997
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "997"
      Then the response status should be "200"
      And the output should be "true"

  Scenario: Send a POST request with parameter is 999
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "999"
      Then the response status should be "200"
      And the output should be "false"

  Scenario: Send a POST request with parameter is 1000
      When I send a POST request to "http://findthebug.herokuapp.com/primenumbers" with parameter key "number" and value "1000"
      Then the response status should be "200"
      And the output should be "false"